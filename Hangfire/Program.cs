﻿using Hangfire;
using Hangfire.MemoryStorage;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.AddHangfire(config => config.UseMemoryStorage()); // Configure Hangfire services https://docs.hangfire.io/
builder.Services.AddHangfireServer();

WebApplication app = builder.Build();

if(app.Environment.IsDevelopment())
{
    // Configure Hangfire dashboard 
    app.UseHangfireDashboard();
}

RecurringJob.AddOrUpdate<MyBackgroundService>("Đây là job Id để phân biệt", x => x.MyJobMethod(), "* * * * *"); // Chạy mỗi phút

app.Run();

public class MyBackgroundService
{
    private readonly ILogger<MyBackgroundService> _logger;

    public MyBackgroundService(ILogger<MyBackgroundService> logger) // Bạn hoàn toàn có thể injection ở đây bình thường
    {
        _logger = logger;
        Console.WriteLine("constructor is called");
    }

    public void MyJobMethod()
    {
        _logger.LogInformation("Hello from Hangfire!");
        _logger.LogError("Hello from Hangfire!");
    }
}
